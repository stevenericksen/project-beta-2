import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent" >
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" id="dropdownmanufacturer" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Manufacturer
            </a>
            <ul className='dropdown-menu' aria-labelledby='dropdownmanufacturer'>
              <li><a className="dropdown-item" href="/manufacturers">Manufacturers</a></li>
              <li><a className="dropdown-item" href="/manufacturers/create">Create a Manufacturer</a></li>
            </ul>
            <a className="nav-link dropdown-toggle" href="#" id="dropdownAutomobiles" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Automobiles
            </a>
            <ul className='dropdown-menu' aria-labelledby='dropdownAutomobiles'>
              <li><a className="dropdown-item" href="/automobiles">Automobiles</a></li>
              <li><a className="dropdown-item" href="/automobiles/create">Create a Automobile</a></li>
            </ul>
          </li>
          <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Vehicle Model
              </a>
              <ul className="dropdown-menu" aria-labelledby='navbarDropdown'>
                <li><a className='dropdown-item' href='/models'>Vehicle Models</a></li>
                <li><a className='dropdown-item' href='/models/new'>Add a Vehicle Model</a></li>
              </ul>
              <a className="nav-link dropdown-toggle" href="#" id="dropdownTechnicians" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Technicians
            </a>
            <ul className='dropdown-menu' aria-labelledby='dropdownTechnicians'>
              <li><a className="dropdown-item" href="/technicians">Technicians</a></li>
              <li><a className="dropdown-item" href="/technicians/create">Create a Technician</a></li>
            </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="dropdownAppointments" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments & Service
              </a>
              <ul className="dropdown-menu" aria-labelledby='dropdownAppointments'>
                <li><a className='dropdown-item' href='/appointments'>Appointments</a></li>
                <li><a className='dropdown-item' href='/appointments/create'>Create a New Appointment</a></li>
                <li><a className='dropdown-item' href='/appointments/history'>Service History</a></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Salespeople
              </a>
              <ul className="dropdown-menu" aria-labelledby='navbarDropdown'>
                <li><a className='dropdown-item' href='/salespeople'>Salespeople List</a></li>
                <li><a className='dropdown-item' href='/salespeople/new'>Add a Salesperson</a></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </a>
              <ul className="dropdown-menu" aria-labelledby='navbarDropdown'>
                <li><a className='dropdown-item' href='/customers'>Customer List</a></li>
                <li><a className='dropdown-item' href='/customers/new'>Add a Customer</a></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby='navbarDropdown'>
                <li><a className='dropdown-item' href='/sales'>Sales List</a></li>
                <li><a className='dropdown-item' href='/sales/new'>Record a sale</a></li>
                <li><a className='dropdown-item' href='/sales/history'>Sales History</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
