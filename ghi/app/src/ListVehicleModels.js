import { useEffect, useState } from 'react';


function VehicleModelList() {
    const [vehicleModels, setVehicleModels] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
            const data = await response.json();
            setVehicleModels(data.models)
            console.log(data)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <>
            <div>
                <h1>Models</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {vehicleModels.map(vehicleModel => {
                            return (
                                <tr key={vehicleModel.href}>
                                    <td>{ vehicleModel.name }</td>
                                    <td>{ vehicleModel.manufacturer.name }</td>
                                    <td><img src={vehicleModel.picture_url} alt={vehicleModel.name} style={{ width: '100px' }}/></td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default VehicleModelList;
