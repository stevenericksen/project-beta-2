import React, { useState, useEffect } from 'react';

function SaleHistory() {
    const [salespeople, setSalespeople] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState('');
    const [salesHistory, setSalesHistory] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const salespeopleResponse = await fetch('http://localhost:8090/api/salespeople/');
                const salesResponse = await fetch('http://localhost:8090/api/sales/');

                if (salespeopleResponse.ok && salesResponse.ok) {
                    const salespeopleData = await salespeopleResponse.json();
                    const salesData = await salesResponse.json();

                    setSalespeople(salespeopleData.salespeople);
                    setSalesHistory(salesData.sales);
                }
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchData();
    }, []);

    const handleSalespersonChange = (e) => {
        const selectedId = e.target.value;
        setSelectedSalesperson(selectedId);
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Salesperson History</h1>
                    <div className="mb-3">
                        <select className="form-select" value={selectedSalesperson} onChange={handleSalespersonChange}>
                            <option value="">Select a Salesperson...</option>
                            {salespeople.map(salesperson => (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            ))}
                        </select>
                    </div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>Automobile</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {salesHistory.filter(sale => sale.salesperson.id.toString() === selectedSalesperson)
                                .map(sale => (
                                    <tr key={sale.id}>
                                        <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                        <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                        <td>{sale.automobile.vin}</td>
                                        <td>${sale.price.toFixed(2)}</td>
                                    </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default SaleHistory;
