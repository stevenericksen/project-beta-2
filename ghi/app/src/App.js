import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ManufacturerForm from './ManufacturerForm'
import ManufacturerList from './ManufacturersList'
import AutomobilesList from './AutomobilesList'
import AutomobileForm from './AutomobileForm';
import Nav from './Nav';
import VehicleModelList from './ListVehicleModels';
import ModelForm from './ModelForm';
import TechniciansList from './TechniciansList';
import TechniciansForm from './TechniciansForm';
import SalespersonForm from './SalespersonForm';
import SalespersonList from './SalespersonList';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import RecordSaleForm from './SaleForm';
import AppointmentsList from './AppointmentsList';
import AppointmentForm from './AppointmentForm';
import ServiceHistoryList from './ServiceHistoryList';
import SaleHistory from './SaleHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="create" element={<ManufacturerForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList />} />
            <Route path="create" element={<AutomobileForm />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleModelList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechniciansList />} />
            <Route path="create" element={<TechniciansForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsList />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistoryList />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<RecordSaleForm />} />
            <Route path="history" element={<SaleHistory />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespersonList />} />
            <Route path="new" element={<SalespersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
