import React, {useState, useEffect} from 'react';
import { useNavigate } from 'react-router-dom';

function ModelForm() {
    const [manufacturers, setManufacturer] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        picture_url:'',
        manufacturer_id: '',
    })
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturer(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const navigate = useNavigate();

    const handleSubmit = async (event) => {

        event.preventDefault();
        console.log('data: ', formData);
        const payload = {
            ...formData,
            manufacturer_id: parseInt(formData.manufacturer_id)
        };
        const url = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            navigate('/models');
        }
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-t mt-4">
                <h1>Add a Vehicle Model</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={formData.name} />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="Picture URL" required type="url" name="picture_url" it="picture_url" className="form-control" value={formData.picture_url} />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-1">
                            <select onChange={handleFormChange} required name="manufacturer_id" id="manufacturer_id" className="form-control" value={formData.manufacturer_id} >
                                <option value="">Choose a manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button type="submit">Add a Vehicle Model</button>
                    </form>
            </div>
            </div>
        </div>
    );
}

export default ModelForm;
