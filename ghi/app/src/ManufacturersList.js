import React, {useEffect, useState} from 'react';

function ManufacturerList(){

    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";

        try {
            const response = await fetch(url);
            if(response.ok){
                const data = await response.json();
                setManufacturers(data.manufacturers)
                console.log(data.manufacturers)
            }
        }catch(e){
            console.log(e);
        }
    };

    useEffect(() => {
        fetchData();
    }, [])


    return (
        <>
        <h1>Manufacturers</h1>
        <table className="table table-striped">
            <thead>
                <tr className="">
                    <th scope="col">Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturer => {
                    return(
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                        </tr>
                    )
                })}
            </tbody>

        </table>
        </>
    )
}

export default ManufacturerList
