import React, {useEffect, useState} from 'react';

function TechniciansList(){

    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";

        try {
            const response = await fetch(url);
            if(response.ok){
                const data = await response.json();
                setTechnicians(data.technicians)
                console.log(data.technicians)
            }
        }catch(e){
            console.log(e);
        }
    };

    useEffect(() => {
        fetchData();
    }, [])
    

    return (
        <>
        <h1>Technicians</h1>
        <table className="table table-striped">
            <thead>
                <tr className="">
                    <th scope="col">Employee ID</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                </tr>
            </thead>
            <tbody>
                {technicians.map(technician => {
                    return(
                        <tr key={technician.id}>
                            <td>{technician.employee_id}</td>
                            <td>{technician.first_name}</td>
                            <td>{technician.last_name}</td>
                        </tr>
                    )
                })}
            </tbody>

        </table>
        </>
    )
}


export default TechniciansList;
