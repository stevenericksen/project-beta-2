import React, {useEffect, useState} from 'react';

function AppointmentsList(){

    const [appointments, setAppointments] = useState([])
    const [automobiles, setAutomobiles] = useState([])

    const fetchData = async () => {
        const appointmentsUrl = "http://localhost:8080/api/appointments/";

        try {
            const response = await fetch(appointmentsUrl);
            if(response.ok){
                const data = await response.json();
                setAppointments(data.appointments)
                console.log(data.appointments)
            }
        }catch(e){
            console.log(e)
        }
    };


    const cancelAppointment = async (id) => {
        const fetchconfig = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
        };

        const cancelResponse = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, fetchconfig);
        if(cancelResponse.ok){
            fetchData();
        }
    };


    const finishAppointment = async (id) => {
        const fetchconfig = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
        };

        const finishResponse = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, fetchconfig);
        if(finishResponse.ok){
            fetchData();
        }
    };


    useEffect(() => {
        fetchData();
    },[])

    const fetchAutoData = async () => {
        const AutomobilesUrl = "http://localhost:8100/api/automobiles/";

        try  {
            const response = await fetch(AutomobilesUrl);
            if(response.ok){
                const autoData = await response.json();
                setAutomobiles(autoData.autos)
                console.log(autoData.autos)
            }
        }catch(e){
            console.log(e)
        }
    };

    useEffect(() => {
        fetchAutoData();
    },[])

    const object = {}
    automobiles.map(automobile => {
        object[automobile.vin] = automobile.sold
    })

    console.log(object)


    return(
        <>
        <h1>Service Appointments</h1>
        <table className="table table-striped">

            <thead>
                <tr className="">
                    <th scope="col">VIN</th>
                    <th scope="col">iS VIP?</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time</th>
                    <th scope="col">Technician</th>
                    <th scope="col">Reason</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    if(appointment.status !== "canceled" && appointment.status !== "finished") {
                    return(
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{object[appointment.vin] ? "Yes" : "No"}</td>
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString([], {hour12: true})}</td>
                            <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                <div className="text-center mb-3">
                                    <button onClick={() => cancelAppointment(appointment.id)} className="btn btn-danger">Cancel</button>
                                    <button onClick={() => finishAppointment(appointment.id)} className="btn btn-success">Finish</button>
                                </div>
                            </td>
                        </tr>
                    )
                    }
                })}
            </tbody>
        </table>
        </>
    )
}

export default AppointmentsList
