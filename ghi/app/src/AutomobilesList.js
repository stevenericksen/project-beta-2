import React, {useEffect, useState} from 'react';

function AutomobilesList(){

   const [automobiles, setAutomobile] = useState([]);

    const fetchData = async () => {
    const url = "http://localhost:8100/api/automobiles/";

    try {
        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setAutomobile(data.autos)
            console.log(data.autos)
        }
    }catch(e){
        console.log(e);
    }
};

useEffect(() => {
    fetchData();
}, [])

    return(
        <>
        <h1>Automobiles</h1>
        <table className="table table-striped">
            <thead>
                <tr className="">
                    <th scope="col">VIN</th>
                    <th scope="col">Color</th>
                    <th scope="col">Year</th>
                    <th scope="col">Model</th>
                    <th scope="col">Manufacturer</th>
                    <th scope="col">Sold</th>
                </tr>
            </thead>
            <tbody>
                {automobiles.map(automobile => {
                    return(
                        <tr key={automobile.id}>
                            <td>{automobile.vin}</td>
                            <td>{automobile.color}</td>
                            <td>{automobile.year}</td>
                            <td>{automobile.model.name}</td>
                            <td>{automobile.model.manufacturer.name}</td>
                            <td>{automobile.sold ? "Yes" : "No"}</td>
                        </tr>
                    )
                })}
            </tbody>

        </table>
        </>
    )
}


export default AutomobilesList;
