import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function RecordSaleForm() {
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [formData, setFormData] = useState({
        salespersonId: '',
        customerId: '',
        automobileVin: '',
        price: ''
    });

    const getSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data.salespeople);
            setSalespeople(data.salespeople);
        }
    }

    const getCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data.customers)
            setCustomers(data.customers);
        }
    }

    const getAutomobiles = async () => {
        try {
            const url = 'http://localhost:8100/api/automobiles/';
            const response = await fetch(url);
            console.log(response);
            if (response.ok) {
                const data = await response.json();
                const unsoldAutomobiles = data.autos.filter(auto => auto.sold === false)
                setAutomobiles(unsoldAutomobiles);
            } else {
                console.error('Failed', response.status);
            }
        } catch (error) {
            console.error('Error', error);
        }
    }

    useEffect(() => {
        getSalespeople();
        getCustomers();
        getAutomobiles();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    };
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();

        const salePayload = {
            salespersonId: parseInt(formData.salespersonId, 10),
            customerId: parseInt(formData.customerId, 10),
            automobileVin: formData.automobileVin,
            price: parseFloat(formData.price)
        }

        const saleUrl = 'http://localhost:8090/api/sales/';
        const saleFetchConfig = {
            method: 'post',
            body: JSON.stringify(salePayload),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const saleResponse = await fetch(saleUrl, saleFetchConfig)

        if (saleResponse.ok) {
            const updateAutoUrl = `http://localhost:8100/api/automobiles/${formData.automobileVin}/`;
            const updateAutoPayload = { sold: true };
            const updateFetchConfig = {
                method: 'PUT',
                body: JSON.stringify(updateAutoPayload),
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const updateResponse = await fetch(updateAutoUrl, updateFetchConfig);
            if (updateResponse.ok) {
                navigate('/sales');
            } else {
                console.error('Failed');
            }
        } else {
            console.error('Failed to create sale')
        }
    };
    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Record a new sale</h1>
                <form onSubmit={handleSubmit}>
                    <div className="mb-1">
                        <select name="salespersonId" onChange={handleFormChange} value={formData.salespeopleId}>
                            <option value="">Choose a salesperson...</option>
                            {salespeople.map(salesperson => (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            ))}
                        </select>
                    </div>
                    <div className="mb-1">
                        <select name="automobileVin" onChange={handleFormChange} value={formData.automobileVin}>
                            <option value="">Choose a automobile VIN...</option>
                            {automobiles.map(automobile => (
                                <option key={automobile.vin} value={automobile.vin}>
                                    {automobile.year} {automobile.color} - {automobile.vin}
                                </option>
                            ))}
                        </select>
                    </div>
                    <div className="mb-1">
                        <select name="customerId" onChange={handleFormChange} value={formData.customerId}>
                            <option value="">Choose a customer...</option>
                            {customers.map(customer => (
                                <option key={customer.id} value={customer.id}>
                                    {customer.first_name} {customer.last_name}
                                </option>
                            ))}
                        </select>
                    </div>
                    <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="Price" required type="number" name="price" id="price" className="form-control" value={formData.price} step="0.01" />
                            <label htmlFor="price">Price</label>
                    </div>
                    <button type="submit">Record sale</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default RecordSaleForm;
