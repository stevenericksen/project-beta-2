import React, {useEffect, useState} from 'react';

function ServiceHistoryList(){

    const [appointments, setAppointments] = useState([])
    const [automobiles, setAutomobiles] = useState([])
    const [vin , setVin] = useState('');

    const fetchData = async () => {
        const appointmentsUrl = "http://localhost:8080/api/appointments/";

        try {
            const response = await fetch(appointmentsUrl);
            if(response.ok){
                const data = await response.json();
                setAppointments(data.appointments)
                // console.log(data.appointments)
            }
        }catch(e){
            console.log(e)
        }
    };

    useEffect(() => {
        fetchData();
    },[])

    const fetchAutoData = async () => {
        const AutomobilesUrl = "http://localhost:8100/api/automobiles/";

        try  {
            const response = await fetch(AutomobilesUrl);
            if(response.ok){
                const autoData = await response.json();
                setAutomobiles(autoData.autos)
                // console.log(autoData.autos)
            }
        }catch(e){
            console.log(e)
        }
    };

    useEffect(() => {
        fetchAutoData();
    },[])

    const object = {}
    automobiles.map(automobile => {
        object[automobile.vin] = automobile.sold
    })

    // console.log(object)

    const handleSearchBarChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        try{
            const filtered = appointments.filter(appointment => vin===appointment.vin);
            // console.log(filtered)
            // fetchData()
            await fetchData()
            setAppointments(filtered)
            setVin('');

       }catch(e){
        console.log(e)
       }
    }


    return(
        <>
        <h1>Service History</h1>
        <div className="search">
        <input onChange={handleSearchBarChange} type="text" value={vin} style={{width:"1225px", height:"40px"}} placeholder="Search by VIN" name="search" id="search" /><button onClick={handleSubmit} style={{height:"40px"}}>Search</button>
        </div>
        <table className="table table-striped" style={{marginTop:"15px"}}>
            <thead>
                <tr className="">
                    <th scope="col">VIN</th>
                    <th scope="col">iS VIP?</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Date</th>
                    <th scope="col">Time</th>
                    <th scope="col">Technician</th>
                    <th scope="col">Reason</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {
                    return(
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{object[appointment.vin] ? "Yes" : "No"}</td>
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString([], {hour12: true})}</td>
                            <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </>
    )
}

export default ServiceHistoryList;
