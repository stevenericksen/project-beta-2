import React, {useEffect, useState} from 'react';

function AppointmentForm(){

    const [VIN, setVin] = useState('')
    const [costumer, setCostumer] = useState('')
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [reason, setReason] = useState('')
    const [technician, setTechnician] = useState('')
    const [technicians, setTechnicians] = useState([]);


    const handleVINChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCostumerChange = (event) => {
        const value = event.target.value;
        setCostumer(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleSubmit = async(event) => {
        event.preventDefault();

        const data = {};
        data.date_time = `${date}T${time}:00+00:00`;      //"2023-04-20T14:39:00.000Z"
        data.reason = reason;
        data.status = "Created";
        data.vin = VIN;
        data.customer = costumer;
        data.technician = Number(technician);

        console.log(data)

        const appointmentsUrl = "http://localhost:8080/api/appointments/";

        const fetchConfig = {
            method:"post",
            body:JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentsUrl, fetchConfig);
        if(response.ok){
            const newAppointment = await response.json();
            console.log(newAppointment)

            setVin('');
            setCostumer('');
            setDate('');
            setTime('');
            setTechnician('');
            setReason('');
        }
    }



    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setTechnicians(data.technicians)
        };
    };

    useEffect(() => {
        fetchData();
    },[])


    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Service Appointment</h1>
            <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
                <input onChange={handleVINChange} value={VIN} placeholder="Automobile VIN" required type="text" name="automobileVin" id="automobileVin" className="form-control" />
                <label htmlFor="automobileVin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleCostumerChange} value={costumer} placeholder="Costumer" type="text" name="costumer" id="costumer" className="form-control" />
                <label htmlFor="costumer">Costumer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateChange} value={date} placeholder="mm/dd/yyyy" required type="date" name="date" id="date" className="form-control" />
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTimeChange} value={time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                <label htmlFor="time">Time</label>
              </div>
              <div className="mb-3">
                <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className="form-select">
                  <option value="">Choose a technician</option>
                  {technicians.map(technician => {
                    return (
                        <option key={technician.id} value={technician.id}>
                            {`${technician.first_name} ${technician.last_name}`}
                        </option>
                    )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                <label htmlFor="reason">Reason</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default AppointmentForm;
