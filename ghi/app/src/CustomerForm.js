import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    };
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();

        const url = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            navigate('/customers');
        }
    };
    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" value={formData.first_name} />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" value={formData.last_name} />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-1">
                            <textarea onChange={handleFormChange} placeholder="Address" required name="address" id="address" className="form-control" value={formData.address} rows="3"></textarea>
                            <label htmlFor='address'>Address</label>
                        </div>
                        <div className="form-floating mb-1">
                            <input onChange={handleFormChange} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" value={formData.phone_number} />
                            <label htmlFor='phone_number'>Phone Number</label>
                        </div>
                        <button type="submit">Add customer</button>
                    </form>
            </div>
            </div>
        </div>
    );
}

export default CustomerForm;
