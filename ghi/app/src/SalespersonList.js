import React, { useState, useEffect } from 'react';

function SalespersonList() {
    const [salespeople, setSalespeople] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            console.log(data.salespeople);
            setSalespeople(data.salespeople);
        }
    }

    const handleDelete = async (salespersonId) => {
        const url = `http://localhost:8090/api/salespeople/${salespersonId}/`;
        const response = await fetch(url, {
            method: "DELETE",
        });

        if (response.ok) {
            getData();
        } else {
            console.error('Failed to delete salesperson');
        }
    }
    useEffect(() => {
        getData()
    }, []);

    return (
        <>
            <div>
                <h1>Salespeople</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Employee ID</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        {salespeople.map(salesperson => {
                            return (
                                <tr key={salesperson.id}>
                                    <td>{ salesperson.first_name }</td>
                                    <td>{ salesperson.last_name }</td>
                                    <td>{salesperson.employee_id}</td>
                                    <td><button className="btn btn-danger" onClick={() => handleDelete(salesperson.id)}>Delete</button></td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default SalespersonList;
