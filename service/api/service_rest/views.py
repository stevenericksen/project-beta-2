from django.shortcuts import render
from django.http import JsonResponse
from .models import Technician, Appointment, AutomobileVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["id", "first_name", "last_name", "employee_id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["id", "vin", "status", "date_time", "technician", "customer", "reason"]

    encoders = {
        "technician": TechnicianListEncoder()
    }

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ["id", "vin", "status", "reason", "technician"]

    encoders = {
        "technician": TechnicianListEncoder()
    }



@require_http_methods(["GET",  "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians},
                            encoder=TechnicianListEncoder,
                            safe=False
                            )
    else:
        try:
           content = json.loads(request.body)
           technician = Technician.objects.create(**content)
           return JsonResponse(
               technician,
               encoder=TechnicianListEncoder,
               safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technicier"}
            )
            response.status_code = 400
            return response



@require_http_methods(["DELETE"])
def api_list_technician(request, id):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"},
                                status=404)



@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({"appointments": appointments},
                            encoder=AppointmentListEncoder,
                            safe=False
                            )
    else:
        try:
            content = json.loads(request.body)
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe= False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the appointment"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_show_appointment(request, id):
    if(request.method) == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"},
                                status=404)




@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    if(request.method) == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "canceled"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    if(request.method) == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "finished"
            appointment.save()

            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
